import firebase from 'firebase'
import firestore from 'firebase/firestore'


// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCt5WNiaImAltrDbU3ggLz-HTUzU4OK-28",
  authDomain: "thebolandchain.firebaseapp.com",
  databaseURL: "https://thebolandchain.firebaseio.com",
  projectId: "thebolandchain",
  storageBucket: "thebolandchain.appspot.com",
  messagingSenderId: "1079543854006",
  appId: "1:1079543854006:web:e2aabfa823a4a263e1c717"
};

// Initialise Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

// export firestore database
export default firebaseApp.firestore()