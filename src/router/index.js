import Vue from 'vue'
import VueRouter from 'vue-router'
import Buefy from 'buefy'

import 'buefy/dist/buefy.css'

// Components
import Index from '@/components/Index.vue'
import Rsvp from '@/components/Rsvp.vue'
import AddGuest from '@/components/AddGuest.vue'
import ViewGuestStatus from '@/components/ViewGuestStatus.vue'


Vue.use(VueRouter)
Vue.use(Buefy)

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index
  },
  {
    path: '/rsvp',
    name: 'Rsvp',
    component: Rsvp
  },
  {
    path: '/add-guest',
    name: 'AddGuest',
    component: AddGuest
  },
  {
    path: '/view-guest',
    name: 'ViewGuestStatutus',
    component: ViewGuestStatus
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
